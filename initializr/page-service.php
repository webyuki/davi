<?php get_header(); ?>

<main>

<section class="bgMainColor pageHeader">
	<div class="container">
		<div class="text-center white">
			<div class="inlineBlock">
				<p class="fontEn h00 titleBdWhite mb30">SERVICE</p>
				<h3 class="h3">仕事内容</h3>
			</div>
		</div>
	</div>
</section>

<section class="margin">
	<div class="container">
		<h3 class="h3 mb30 text-center">品質は、材料・製造・検査の集大成</h3>
		<div class="width720">

<p>岡田精工は、安定した材料の仕入れと
確かな技術を持つ職人による加工、
厳しい検査によって高品質な製品をご提供しています。		
</p>			
		</div>
	</div>
</section>

<section class="relative">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_message_02.jpg">
		<div class="padding">
			<div class="container">
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
					
						<div class="topServiceBoxCol white bgMainColor boxBd mb30">
							<img class="top_service_ico mb20" src="<?php echo get_template_directory_uri();?>/img/top_service_ico_01.png" alt="">
							<h4 class="h3 titleBdWhite text-center mb20">材料の調達</h4>
							<p class="text_m">岡田精工ではお客さまからの信頼を最優先し、厳しい社内基準に適合する高品質な材料を仕入れています。高い基準を満たす材料を確保することは、製品の品質安定への第一歩。”品質は材料から”を信念とし、製品の一貫した品質管理を行っています。</p>
						</div>

					</div>
					<div class="col-sm-6">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin">
	<div class="container">
	
		<div class="row mb30">
			<div class="col-sm-6" data-aos="fade-right">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_01.jpg" alt="">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<h4 class="h3 bold mainColor mb30">JIS規格にとどまらない自社基準</h4>
				<div>
				
<p>農機具や産業器具の材料となる鉄や、医療機器の材料となるアルミやステンレスは、JIS規格で品質の基準が定められています。日本で使用されるそれらの材料は、ある程度の質の高さが保証されています。</p>

<p>加えて岡田精工では、製品の高い品質を確実にするため、JIS規格を上回る独自の基準を定めて材料を仕入れています。製品を作る一連の流れから考えると、材料は製品の”もと”。確かな材料を使い、安心してお選びいただける製品を製造しています。</p>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_02.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<h4 class="h3 bold mainColor mb30">材料の仕入れは数ケ月前から手配</h4>
				<div>
				
<p>岡田精工では、自社で定めた高い基準を満たす材料を確実に仕入れるため、数ケ月前から材料の手配をしています。お客さまからのご注文に対し、常に安定した品質の製品を製造できるのは、この仕入れの成果です。</p>

<p>一方で国際情勢による材料の不足や、予測できない注文に対して在庫を抱えることなど、事前の発注によるリスクも小さくはありません。それでも岡田精工を信頼してくださるお客さまのために、弊社では今後も材料の確保には力を注いでいく方針です。</p>			
				</div>
			</div>
		</div>
	
	</div>
</section>

<section class="relative">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_message_02.jpg">
		<div class="padding">
			<div class="container">
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
					
						<div class="topServiceBoxCol white bgMainColor boxBd mb30">
							<img class="top_service_ico mb20" src="<?php echo get_template_directory_uri();?>/img/top_service_ico_02.png" alt="">
							<h4 class="h3 titleBdWhite text-center mb20">機械の製作</h4>
							<p class="text_m">材料が製品の”もと”であるならば、製造は製品を”育てる”工程。綿密な計画と確かな技術によって、材料の質を高めていきます。今ある知識と技術に満足するのではなく、製造を支える会社全体の品質向上を常に目指します。</p>
						</div>

					</div>
					<div class="col-sm-6">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin">
	<div class="container">
	
		<div class="row mb30">
			<div class="col-sm-6" data-aos="fade-right">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_03.jpg" alt="">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<h4 class="h3 bold mainColor mb30">図面以上のクオリティを提供</h4>
				<div>
				
<p>農機具・産業機械・医療機器は一般的に、新規のご注文を受けた際、お客さまからご提供いただく図面に沿って製品を製造します。岡田精工ではこれに加え、品質管理課と製造課双方で図面の細部を検証し、より高品質な製品をお届けできるよう工夫を重ねています。</p>

<p>多くのお客さまがくり返しご注文をくださるのは、私たちのこれまでの仕事に対する信頼の証。「思っていたよりずっと良かった」の声を支えに、今後も期待を上回る製品をご提供してまいります。</p>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_04.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<h4 class="h3 bold mainColor mb30">高品質な製品を短時間で</h4>
				<div>
				
<p>岡田精工では製品の高い品質をお約束するだけでなく、注文量が大幅に増加した場合にも、同質の製品をスピーディーにご提供できることを重視しています。</p>

<p>同じ製品であっても常に製造工程の見直しや改善を行い、急な注文でもお客さまには安心してご依頼頂ける環境を整えています。「なんとかならないか」のご要望に幾度となく応えてきた社内の体制も、満足することなく改善を重ねています。</p>			
				</div>
			</div>
		</div>
	
	</div>
</section>


<section class="relative">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_message_02.jpg">
		<div class="padding">
			<div class="container">
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
					
						<div class="topServiceBoxCol white bgMainColor boxBd mb30">
							<img class="top_service_ico mb20" src="<?php echo get_template_directory_uri();?>/img/top_service_ico_03.png" alt="">
							<h4 class="h3 titleBdWhite text-center mb20">製造後の検査</h4>
							<p class="text_m">岡田精工では、品質管理課が工程の初期段階から製造に関わります。また初品のダブルチェックや検品者の技術向上など、品質管理の要として、様々な面から検査精度の向上に努めています。</p>
						</div>

					</div>
					<div class="col-sm-6">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin">
	<div class="container">
	
		<div class="row mb30">
			<div class="col-sm-6" data-aos="fade-right">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_05.jpg" alt="">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<h4 class="h3 bold mainColor mb30">すべての製品はダブルチェックからスタート</h4>
				<div>
				
<p>弊社では、お客さまからのご要望を上回る厳しい自社基準を定めており、完成した製品は細部まで検査をしています。特に最初に完成した製品に対しては、加工者と加工者以外のスタッフが厳重なダブルチェックを行います。</p>

<p>また品質管理課は、図面をお客さまからお預かりした段階から関わることで、通常であれば製造後に行う検査の内容をより詳しく把握。ひとつの製品に関わるスタッフを増やすことで、小さな異常も見逃さない体制づくりをしています。</p>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_06.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<h4 class="h3 bold mainColor mb30">経験と技術を重ねることで、検査精度を向上</h4>
				<div>
				
<p>弊社では基本的に、製品の加工に携わったスタッフが製造後の検査を行っています。この時必要となるのが、正確性とスピード。製造した製品を誰よりもよく知る加工者には、さらに厳しい目ですべての製品を正確に検査することが求められます。</p>

<p>私たちは加工の技術を向上させることは検査の能力を向上させ、検査の能力を上げることは加工の技術を磨くことにつながると考えています。現場でのくり返しと積み重ねが、岡田精工の品質を守っています。</p>			
				</div>
			</div>
		</div>
	
	</div>
</section>


<section class="bgSubColor padding">
	<div class="container">
		<div class="text-center mainColor mb50">
			<div class="inlineBlock">
				<p class="fontEn h00 titleBd mb30">CAREER</p>
				<h3 class="h3">キャリアアップ</h3>
			</div>
		</div>
		<div class="width720 mb50">
		
<p>岡田精工は、金属加工業で培った技術力と
産業用機械の製作のノウハウを生かし、
お客様の快適な暮らしを支えるものづくりを続けてまいりました。</p>

		</div>
		
		<div class="row" data-aos="fade-right">
			<div class="col-sm-4">
				<div class="topServiceBoxCol white bgMainColor boxBd mb30">
					<img class="page_service_ico mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_ico_01.png" alt="">
					<h4 class="h4 titleBdWhite text-center mb20">先輩社員の作業補助</h4>
					<p class="text_m">配属後、先輩社員について機械の操作方法を覚えます。監督者が可能であると判断したのち、単独で機械操作を担当します。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBoxCol white bgMainColor boxBd mb30">
					<img class="page_service_ico mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_ico_02.png" alt="">
					<h4 class="h4 titleBdWhite text-center mb20">現場で活躍</h4>
					<p class="text_m">1台～複数台の機械操作を担当します。経験や技術の向上に合わせて、複雑な加工や高い精度が要求される製品も担当します。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBoxCol white bgMainColor boxBd mb30">
					<img class="page_service_ico mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_ico_03.png" alt="">
					<h4 class="h4 titleBdWhite text-center mb20">責任者として現場を担当</h4>
					<p class="text_m">機械や工具、工程の設定、プログラムの作成を行います。生産精度と効率アップに向けて製造工程全体を管理します。</p>
				</div>
			</div>
		</div>
		
	</div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>