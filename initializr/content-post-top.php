<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>

                <li class="">
                    <a href="<?php the_permalink();?>" class="flex topNewsFlex">
                        <span class="gray text_m"><?php the_time('y/m/d'); ?></span>
                        <span class="mainColorLight text_m"><?php echo $cat_name; ?></span>
                        <span class=""><?php the_title();?></span>
                    </a>
                </li>