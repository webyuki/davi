<?php
//アイキャッチ画像表示
add_theme_support('post-thumbnails'); 
function custom_theme_thumbnails() {
	set_post_thumbnail_size(); // 通常の投稿サムネイル
	add_image_size('thumbnailSmall' ,100, 100 ,true ); // オリジナルサムネイルサイズ 小
	add_image_size('thumbnailMidium',250, 250 ,true ); // オリジナルサムネイルサイズ 中
	add_image_size('thumbnailBig'  ,500, 500 ,true ); // オリジナルサムネイルサイズ 大
	add_image_size('thumb_size_m', 800, 556,true );
	add_image_size('thumb_size_s', 339, 225);

}
add_action('after_setup_theme', 'custom_theme_thumbnails');

//グローバルメニュー
 register_nav_menu('mainmenu', 'メインメニュー');

//メニューの「タイトル属性」を出力させる対応
add_filter('walker_nav_menu_start_el', 'description_in_nav_menu', 10, 4);
 
function description_in_nav_menu($item_output, $item){
  return preg_replace('/(<a.*?>[^<]*?)</', '$1' . "<span>{$item->attr_title}</span><", $item_output);
}


// パンくずリスト
function breadcrumb(){
    global $post;
    $str ='';
    if(!is_home()&&!is_admin()){
        $str.= '<div id="breadcrumb" class="cf"><div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
        $str.= '<a href="'. home_url() .'" itemprop="url"><span itemprop="title">ホーム</span></a> &gt;</div>';
 
        if(is_category()) {
            $cat = get_queried_object();
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($ancestor) .'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor) .'</span></a> &gt;</div>';
                }
            }
        $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($cat -> term_id). '" itemprop="url"><span itemprop="title">'. $cat-> cat_name . '</span></a></div>';
        } elseif(is_page()){
            if($post -> post_parent != 0 ){
                $ancestors = array_reverse(get_post_ancestors( $post->ID ));
                foreach($ancestors as $ancestor){
                    $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_permalink($ancestor).'" itemprop="url"><span itemprop="title">'. get_the_title($ancestor) .'</span></a></div>';
                }
            }
        } elseif(is_single()){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($ancestor).'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor). '</span></a> &gt;</div>';
                }
            }
            $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($cat -> term_id). '" itemprop="url"><span itemprop="title">'. $cat-> cat_name . '</span></a></div>';
        } else{
            $str.='<div>'. wp_title('', false) .'</div>';
        }
        $str.='</div>';
    }
    echo $str;
}

//カテゴリーで親カテゴリー以下で表示を指定する指示
function post_is_in_descendant_category( $cats, $_post = null ){
   foreach ( (array) $cats as $cat ) {
         $descendants = get_term_children( (int) $cat, 'category');
          if ( $descendants && in_category( $descendants, $_post ) )
              return true;
   }
   return false;
}

add_filter('the_content', 'wpautop');
add_filter('the_excerpt', 'wpautop');


/*管理画面でカスタムタクソノミー*/
add_action( 'restrict_manage_posts', 'add_post_taxonomy_restrict_filter' );
function add_post_taxonomy_restrict_filter() {
    global $post_type;
    if ( 'menu' == $post_type ) {
        ?>
        <select name="menu_tax">
            <option value="">カテゴリー指定なし</option>
            <?php
            $terms = get_terms('menu_tax');
            foreach ($terms as $term) { ?>
                <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
            <?php } ?>
        </select>
        <?php
    }
}
add_action('manage_posts_custom_column', 'add_custom_column_id', 10, 2);

register_sidebar();

function sampletheme_widgets_init() {
	register_sidebar( array(
		'name' => 'ブログサイドバー',
		'id' => 'sidebar-blog',
		'description' => 'ブログサイドバー',
		'before_widget' => '<aside class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<p class="quattro italic h0 bold gray">',
		'after_title' => '</p>'
		
	) );
	register_sidebar( array(
		'name' => 'フッター1',
		'id' => 'footer1',
		'description' => 'フッター1',
		'before_widget' => '<aside class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<p class="quattro italic h0 bold gray">',
		'after_title' => '</p>'
		
	) );
}

add_action( 'widgets_init', 'sampletheme_widgets_init');
 