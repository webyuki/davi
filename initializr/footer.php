<section id="shop" class="bgImg section relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_shop_bg.jpg');">
    <div class="bgBlackTrans fullpageInner paddingW">
        <div class="container">
            <div class="width720 topConceptBox relative shop">
                <div class="topConceptBoxLineWrap relative">
                    <div class="topConceptBoxLine">
                        <div class="text-center">
                            <p class="mainColor fontEn h1 mb20">Shop</p>
                        </div>
                        <div class="footerShopInnerText">
                            <p class="mainColor fontEn text-center mb10 h3">Davi su misura</p>
                            <p class="mainColor serif mb30 footerShopAddres text-center">
                                <span class="footerAddress"><a class="link fontNum" href="https://goo.gl/maps/t313v6TckydHdXWx5" target="_blank">5116-35 Haga Kita-ku,<br>Okayama city, Okayama<br>701-1221, Japan</a></span><br>
                                <span class="">Tel：<a class="link sp fontNum" href="tel:0862841085">+81 86 284 1085</a></span>
                                <span class="pc fontNum">+81 86 284 1085</span><br>
                                <span><a class="link" href="https://www.instagram.com/davi_su_misura/" target="_blank">Instagram</a></span><br>
                            </p>
                        </div>
                        <div class="text-center">
                            <a href="<?php echo home_url();?>/contact" class="button h4 fontEn tra text-center">CONTACT</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper_copy absolute">
            <div class="container">
                <p class="text-center text_s white">copyright© 2019 Davi su misura all rights reserved.</p>
            </div>
        </div>
    </div>
</section>

<!--
<section class="relative footerShop section" id="shop">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_shop_bg.jpg">
        <div class="bgBlackTrans paddingW">
            <div class="container">
                <div class="bdBox width720 topConceptBox relative shop">
                    <div class="topConceptBoxLineWrap relative">
                        <div class="topConceptBoxLine">
                            <div class="text-center mb30">
                                <p class="mainColor fontEn h1 titleBd inlineBlock mb10">Shop</p>
                            </div>
                            <div>
                                <p class="mainColor fontEn mb20 h3">Davi su misura</p>
                                <p class="mainColor serif mb50">
									<a class="link" href="https://goo.gl/maps/t313v6TckydHdXWx5" target="_blank">〒701-1221<br>岡山県岡山市北区芳賀5116-35</a><br>
									Tel：<a class="link sp" href="tel:0862841085">086-284-1085</a>
									<span class="pc">086-284-1085</span>
								</p>
                            </div>
                            <div class="text-center mb30">
                                <a href="<?php echo home_url();?>/contact" class="button h3 fontEn tra text-center">CONTACT</a>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper_copy absolute">
            <div class="container">
                <p class="text-center text_s gray">copyright© 2019 Davi su misura all rights reserved.</p>
            </div>
        </div>
    </div>
</section>
-->

</div>

<style>/*
<section class="sectionPankuzu">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
	</div>
</section>
*/</style>

<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement : "bottom-top",
	duration: 700,
    easing : "ease-out-sine"
});
</script>
</body>
</html>
 