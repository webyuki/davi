<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!--キャッシュクリア-->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!--キャッシュクリア終了-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo home_url();?>/favicon.ico" />
<title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo home_url();?>/apple-touch-icon-180x180.png">
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/modal.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/loaders.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/lightbox/dist/css/lightbox.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Ibarra+Real+Nova&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Neuton:300&display=swap" rel="stylesheet">


<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/slick/slick.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/slick/slick-theme.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/fullpage/dist/fullpage.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/iziModal/css/iziModal.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/main.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">

<script src="<?php echo get_template_directory_uri();?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri();?>/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/fullpage/vendors/scrolloverflow.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/fullpage/dist/fullpage.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/iziModal/js/iziModal.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/lightbox/dist/js/lightbox.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.matchHeight-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/parallax/parallax.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>
<script>
/*高さ合わせる*/
$(function(){
	$('.matchHeight').matchHeight();
});

</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-59KG5Z2');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class();?>>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-59KG5Z2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="loadingBg">
	<div class="loading-Bar"></div>
	<img class="loading-Img" src="<?php echo get_template_directory_uri();?>/img/logo_lucolt.png" alt="ルコルト" style="display:none;">
</div>

<style>
.loadingBg{
	width: 100vw;
	height: 100vh;
	position: fixed;
    z-index: 1002;
    background-color: #f7f7f7;
}
img.loading-Img {
	max-width: 70px;
    z-index: 999;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
} 
.loading-Bar {
  position: fixed;
  top: 0;
  left: 0;
  background: #111111;
  height: 4px;
  -webkit-transition: all 0.4s linear 0s;
  transition: all 0.4s linear 0s;
  width: 0;
}
 
.loading-Img {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
}

@media screen and (max-width:767px){
    img.loading-Img {
        max-width: 60px;
    } 

}

</style>

<script>

$(function(){
 $(".header").css("display","none");
 $(".contents").css("display","none");
});
 
 
$(function(){
 
  // loading状況の初期化
  // loadCount：読み込まれていく画像の数をカウントする変数
  var loadCount = 0;
  // なんかの初期化
  var aCnt = 0;
 
  // ローディング中、中央に現れる画像
  $(".loading-Img").delay(300).fadeIn("slow");
 
  // imgが読み込まれるタイミングを監視
  // Edgeと、IEキャッシュ問題の解消
  // imgタグそれぞれに対して、functionの中を実行しなさいという命令
  $("img").each(function(){
 
    // イメージオブジェクトを作成している
    // $(this)は、ある１つのimgタグを指しているので、そのimgタグのsrcをイメージオブジェクトのsrcに設定
    // これにより、イメージオブジェクトのwidthで、画像が読み込まれたときに、imgタグのwidthと連動することになる
    // なぜ、widthを見るか、読み込まれの指標としたいから
    var img = new Image();
    img.src = $(this).attr("src");
    // イメージオブジェクトがwidthを持つまで、widthが 0以上かどうかを調べるためのタイマー
    // cntは、タイマーの繰り返し回数を数えていて、１０回を超えたら画像が全て読み込まれたものとする
    // 画像が読み込まれるのが失敗することもあるかもしれないので、そのときには、バーを伸ばしてスルーする
    var cnt = 0;
    var timer = setInterval(function(){
      cnt++;
 
      // || は or
      // これは、画像が読み込まれたら、つまり、widthが0以上の値をとった時の処理をわけている
      // 画像が読み込まれたら、タイマーを止めています。
      if(img.width > 0 || cnt > 10){
        cnt = 0;
        // setInterval内の関数を止める処理 clearInterval
        clearInterval(timer);
 
        // 読み込む画像の数を取得
        // これで、読み込まれた画像の数と、ページの全体の数が同じなら、全部画像が読み込まれているのがわかるので
        // コンテンツをフェードイン、メニューをスライドダウンなどの処理のタイミングの指標になる
        imgLength = $("img").length * 10; //imgLengthは170読み込まれることになる
 
        loadCount++;//1~17枚まで増える
 
        $(".loading-Bar").css({
          //読み込まれた画像の数を画像全体で割り、%としてローディングバーのwidthに設定
          "width": (loadCount / imgLength) * 100 + "%"
        });
      }
    }, 50);
    // タイマーは50msで動いている。画像がキャッシュにあっても50ms間隔で画像の数、線は段階的に１００％まで伸びて行く
 
    var tndST = setInterval(function(){
      loadCount++;
      aCnt++;
 
    if(aCnt > 9){
      clearInterval(tndST);
      aCnt = 0;
 
      } else {
        $(".loading-Bar").css({
          //読み込まれた画像の数を画像全体で割り、%としてローディングバーのwidthに設定
          "width": (loadCount / imgLength) * 100 + "%"
        });
      }
 
    if(loadCount > imgLength){
        //100%読み込まれたらローディングバーを隠す
        // バーをfadeOutする。
        $(".loading-Bar").delay(50).fadeOut("normal");
        $(".loadingBg").delay(50).fadeOut("normal");
 
        // ロゴをフェードアウトする。
        $(".loading-Img").delay(100).fadeOut("normal");
  
      }
    },100);
    // タイマーは320msで動いている。画像がキャッシュにあっても320ms間隔で画像の数、線は段階的に１００％まで伸びて行く
  });
});
</script>


<header>
    <a class="logoLink" href="<?php echo home_url();?>"><h1 class="logo remove"><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></h1></a>
</header>

<!-- 開閉用ボタン -->

<div class="menu-btn" id="js__btn">
    <!--<span data-txt-menu="MENU" data-txt-close="CLOSE"></span>-->
	<div class="menu-trigger" href="#">
		<span></span>
		<span></span>
		<span></span>
	</div>	
</div>

<!-- モーダルメニュー -->
<nav class="overRayMenu menu bgMainColor" id="js__nav">
    <a class="logoLink" href="<?php echo home_url();?>"><p class="logo remove"></p></a>

	<?php wp_nav_menu( array( 'menu_class' => 'fontEn overRayMenuUl fullpageMenu' ) ); ?>
</nav>






<script>

$(function () {
    $()
});
$(function () {
    var $body = $('body');

    //開閉用ボタンをクリックでクラスの切替え
    $('#js__btn').on('click', function () {
        $body.toggleClass('open');
        $('.menu-trigger').toggleClass('active');
        
    });

    //メニュー名以外の部分をクリックで閉じる
    $('#js__nav').on('click', function () {
        $body.removeClass('open');
        $('.menu-trigger').removeClass('active');
    });
});

</script>

<div id="fullpage">
