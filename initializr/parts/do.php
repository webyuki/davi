<section class="area_cando">
	<div class="container">
		<p class="h3 wrapper_title_top text-center white">私たちにできること</p>
		<div class="wrapper_ul_cando">
			<ul class="inline_block">
				<li><a href="<?php echo home_url();?>/big"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_big.jpg)">大規模リフォーム</p></a></li>
				<li><a href="<?php echo home_url();?>/earth"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_earth.jpg)">耐震対策</p></a></li>
				<li><a href="<?php echo home_url();?>/roof"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_roof.jpg)">屋根</p></a></li>
				<li><a href="<?php echo home_url();?>/wall"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_wall.jpg)">外壁塗装</p></a></li>
				<li><a href="<?php echo home_url();?>/bath"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_bath.jpg)">お風呂</p></a></li>
				<li><a href="<?php echo home_url();?>/toilet"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_toilet.jpg)">トイレ</p></a></li>
				<li><a href="<?php echo home_url();?>/kitchen"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_kitchen.jpg)">キッチン</p></a></li>
				<li><a href="<?php echo home_url();?>/care"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_care.jpg)">介護リフォーム</p></a></li>
				<li><a href="<?php echo home_url();?>/eco"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_eco.jpg)">エコリフォーム</p></a></li>
				<li><a href="<?php echo home_url();?>/in"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_in.jpg)">内装</p></a></li>
				<li><a href="<?php echo home_url();?>/diplo"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_diplo.jpg)">外構</p></a></li>
				<li><a href="<?php echo home_url();?>/other"><p class="remove shadow" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bn_ico_other.jpg)">その他（電気工事・白蟻駆除・防犯対策）</p></a></li>
			</ul>
		</div>
			
	</div>
</section>




 

