<div class="entry_cont">
	<p>最近はファッション関係で色んなサービスがでてきています。その中でも僕が使っているもの、いいなと思ってるのはこちらです！随時更新していきます。</p>
	
	
	<ul>
		<li>
			<a href="https://px.a8.net/svt/ejp?a8mat=2TVRNI+BB8IK2+3GIS+6GRMQ" target="_blank" rel="nofollow">メンズファッションの通販DHOLIC</a>：女性に人気のディーホリックが意外にセンスが良くて、安い！僕が唯一通販で服を買ってるところです。
		</li>
		<li>
			<a href="https://px.a8.net/svt/ejp?a8mat=2TE8D9+9E68DU+3PAC+5YJRM" target="_blank" rel="nofollow">男性向け月額ファッションレンタル leeap [リープ]</a>：毎月新しい服を買うのが面倒。。。という方向けのレンタルサービス。毎月スタイリストが選んだコーディネートが届きます。
		</li>
		<li>
			<a href="https://px.a8.net/svt/ejp?a8mat=2TKPSE+D2CGOI+3QSI+62ENM" target="_blank" rel="nofollow">男性の服の購入代行【ベストスタイルミーダイレクト】</a>：こちらはレンタルではなく、毎月定期的にコーディネートされた服を購入できるというもの。
		</li>
	</ul>
	
	
	<!--リープ-->
	<img border="0" width="1" height="1" src="https://www14.a8.net/0.gif?a8mat=2TE8D9+9E68DU+3PAC+5YJRM" alt="">
	
	<!--Dholic-->
	<img border="0" width="1" height="1" src="https://www18.a8.net/0.gif?a8mat=2TVRNI+BB8IK2+3GIS+6GRMQ" alt="">
	
	<!--ベストスタイルミー-->
	<img border="0" width="1" height="1" src="https://www11.a8.net/0.gif?a8mat=2TKPSE+D2CGOI+3QSI+62ENM" alt="">
</div>


