<?php
	$prevpost = get_adjacent_post(true, '', true); //前の記事
	$nextpost = get_adjacent_post(true, '', false); //次の記事
	if( $prevpost or $nextpost ){ //前の記事、次の記事いずれか存在しているとき
?>
<ul class="prev-next">
	<?php
		if ( $prevpost ) { //前の記事が存在しているとき
			
			// アイキャッチ画像のIDを取得
			$thumbnail_id = get_post_thumbnail_id($prevpost->ID); 
			// mediumサイズの画像内容を取得（引数にmediumをセット）
			$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
			// 取得した画像URLにてイメージタグを出力
			// 更にdata-aliasというHTML5のカスタムデータ属性を追加
			
			echo '<li><a href="' . get_permalink($prevpost->ID) . '"><div class="prev-next__bg relative matchHeight" style="background-image:url('.$eye_img[0].')"><div class="prev-next__txt relative"><div class="text_m thin mb10">前の記事</div>'. get_the_title($prevpost->ID) .'</div></div></a></li>';
			} else { //前の記事が存在しないとき
			echo '<li><a href="' . network_site_url('/') . '"><div class="prev-next__bg relative matchHeight" style="background-image:url('.get_template_directory_uri().'/img/sample01.png)"><div class="prev-next__txt relative"><div class="text_m thin mb10">TOPへ戻る</div></div></div></a></li>';
			
			
			}
			if ( $nextpost ) { //次の記事が存在しているとき
			
			// アイキャッチ画像のIDを取得
			$thumbnail_id = get_post_thumbnail_id($nextpost->ID); 
			// mediumサイズの画像内容を取得（引数にmediumをセット）
			$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
			// 取得した画像URLにてイメージタグを出力
			// 更にdata-aliasというHTML5のカスタムデータ属性を追加
			
			echo '<li><a href="' . get_permalink($nextpost->ID) . '"><div class="prev-next__bg relative matchHeight" style="background-image:url('.$eye_img[0].')"><div class="prev-next__txt relative	"><div class="text_m thin mb10">次の記事</div>'. get_the_title($nextpost->ID) .'</div></div></a></li>';
			
			} else { //次の記事が存在しないとき
			echo '<li><a href="' . network_site_url('/') . '"><div class="prev-next__bg relative matchHeight" style="background-image:url('.get_template_directory_uri().'/img/sample01.png)"><div class="prev-next__txt relative"><div class="text_m thin mb10">TOPへ戻る</div></div></div></a></li>';
		}
	?>
</ul>
<?php } ?>