<?php get_header(); ?>

<main>
<section class="pageHeader" id="">
    <div class="container" data-aos="fade-up">
        <div class="text-center mb50">
            <p class="fontEn h1 titleBd titleBdBlack inlineBlock mb10">Contact</p>
            <h3 class="serif h3">お問い合わせ</h3>
        </div>
    </div>
</section>



<section class="margin">
	<div class="container">
		<div class="">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>少しでも気になることがございましたら、<br>お気軽にご相談ください。</p>
                    <p>また、ご意見ご質問等も承っております。</p>
				</div>
				<a class="telLink fontEn h0 text-center bold block mb0" href="tel:0862841085">086-284-1085</a>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="61"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>