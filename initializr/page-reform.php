<?php get_header(); ?>

<main>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_reform_fv.jpg">
        <div class="bgWhiteTrans paddingW">
            <div class="container" data-aos="fade-up">
                <div class="text-center">
                    <p class="fontEn h3 mb0 mainColor">REFORM</p>
                    <h3 class="h2 bold">リフォーム</h3>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="padding bgSubColor">
    <div class="container">
        <div class="text-center mb50">
            <h3 class="h3 bold">リフォームでお困りではありませんか？</h3>
        </div>
        <div class="width720 bgWhite" data-aos="fade-up">
            <ul class="pageReformTroubleUl">
                <li><i class="fa fa-check-circle h2 mainColorLight" aria-hidden="true"></i><span class="bold h4">お風呂やトイレの水漏れをなおしたい</span></li>
                <li><i class="fa fa-check-circle h2 mainColorLight" aria-hidden="true"></i><span class="bold h4">住む人にあわせてリフォームしたい</span></li>
                <li><i class="fa fa-check-circle h2 mainColorLight" aria-hidden="true"></i><span class="bold h4">見た目をきれいにしたい</span></li>
            </ul>
        </div>
    </div>
</section>


<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">ADVANTAGE</p>
            <h3 class="h3 bold">オカザキリフォームラボのリフォーム</h3>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_01.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor">01</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">地元玉野で90年の実績</span></h4>
                <div class="mb30">

<p>岡崎建材を前身とするオカザキリフォームラボは、玉野市で90年、住まいのお悩みに対応しています。</p>
<p>創業当時は、ようやく民家にお風呂が作られ始めた時代。新設工事から破損・経年による水漏れの修理、そしてシステムバスの導入まで。町とお住いの歴史とともに、変わりゆくお悩みに寄り添ってきました。</p>
<p>安心で安全な暮らしのために。オカザキリフォームラボはお客さまの生活のパートナーであり続けます。</p>


                </div>
            </div>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_02.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor left">02</p>
                </div>
            </div>
            <div class="col-sm-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">深い商品知識と経験にもとづくご提案</span></h4>
                <div class="mb30">

<p>住まいを支える設備は、日々新しい商品が開発されています。デザインや機能など、メーカーのこだわりは商品によってさまざま。お客さまからは「どれを選んでいいのかわからない」との声が寄せられてきました。</p>
<p>オカザキリフォームラボでは、お客さまにかわって取扱商品をひとつ一つ吟味し、ご要望やご家族、ライフスタイルに適した商品をご提案しています。</p>


                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_about_feature_02.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor">03</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">設計士・デザイナー・職人の広い<br>ネットワーク</span></h4>
                <div class="mb30">

<p>オカザキリフォームラボでは、水まわりだけでなく、外壁のリフォームやエクステリアも手がけています。</p>
<p>90年にわたって建材の販売や施工にかかわってきた経験から、設計士・デザイナー・職人などとの広いネットワークが強み。</p>
<p>バラエティ豊かなお好みや、幅広い世代からのご要望など、細かなリクエストを理想の形に仕上げることが可能です。</p>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding bgMainColorLight">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">POINT</p>
            <h3 class="h3 bold">安心で確実。長く住み続けるためのリフォーム</h3>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_point_01.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">トイレのリフォーム</h4>
                    <p class="text_m gray">便器やウォシュレットの取り付けや交換、水漏れの修理、部品交換はもちろん、クロスやクッションフロアの貼り替えなどにも対応しています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_point_02.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">お風呂のリフォーム</h4>
                    <p class="text_m gray">水漏れの修理やタイルの貼り替えだけでなく、システムバスへの交換工事や、それに伴う付帯工事にも対応しています。水漏れによる建物への影響が心配な方もご相談ください。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_point_03.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">キッチンのリフォーム</h4>
                    <p class="text_m gray">水漏れの修理や部品の交換だけでなく、キッチンを丸ごと新しくするリノベーションにも対応しています。使い勝手の悪さが気になる、ライフスタイルに変化があったなど、細かな要望にお応えします。</p>
                </div>
            </div>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_point_04.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">外壁のリフォーム</h4>
                    <p class="text_m gray">風雨や日照によるひび割れや色あせなど、外壁のリフォームに対応しています。お客さまのご要望に合わせ、建物を長く守るためのご提案をしています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_point_05.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">エクステリアのリフォーム</h4>
                    <p class="text_m gray">お客さまの理想を叶える設計士やデザイナーと連携し、エクステリアを美しく再生します。建材を長く扱ってきた経験をもとに、高品質な材料を選んでいます。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_point_06.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">リノベーション</h4>
                    <p class="text_m gray">水まわりやエクステリアだけでなく、リビングなど居室をふくめたリノベーションに対応しています。設計士やデザイナーと連携し、細やかな要望を実現しています。</p>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="<?php echo home_url();?>/works" class="white button bold tra text-center">リフォームの実績を見る</a>
        </div>
    </div>
</section>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_reform_voice_bg.jpg">
        <div class="bgWhiteTrans padding">
            <div class="container" data-aos="fade-up">
                <div class="text-center mb50">
                    <p class="fontEn h4 mb0 mainColor">VOICE</p>
                    <h3 class="h3 bold">お客様の声</h3>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50 matchHeight">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_voice_02.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">もっと早くお願いすればよかったです。</h5>
                                <p class="grayLight bold">玉野市 N様</p>
                            </div>
                            <p class="text_m gray">ウォシュレットが壊れたので修理をお願いするつもりでしたが、節水や掃除のしやすさを考えて便器ごとウォシュレットも交換しました。交換後は掃除の回数も減ってすごく楽になりました。もっと早くお願いすればよかったと後悔しています。</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50 matchHeight">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">新築のようにきれいになりました。</h5>
                                <p class="grayLight bold">玉野市 O様</p>
                            </div>
                            <p class="text_m gray">タイルから水が漏れるようになり、シロアリも心配だったので思い切ってユニットバスにリフォームしました。今のお風呂ってあたたかいんですね！冬でもゆっくりバスタイムを楽しんでいます。明るくてきれいで、お風呂だけは新築みたいです（笑）</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50 matchHeight">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_voice_03.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">とても腕のいい職人さんでした。</h5>
                                <p class="grayLight bold">岡山市 S様</p>
                            </div>
                            <p class="text_m gray">ゴミが散らかることもなく、隅々まで丁寧に工事してもらいました。キッチンのリフォームだったので浄水器や棚の取り付けが気になっていましたが、ガタつきやゆるみもなく安心して使えています。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding bgSubColor">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">FLOW</p>
            <h3 class="h3 bold">リフォームの流れ</h3>
        </div>
        <div class="width980 bgWhite pageReformFlowWrap" data-aos="fade-up">
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_01.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">お問い合わせ</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 01</p>
                    <p>お電話やWebサイトのお問い合わせフォームからご連絡ください。パソコン・スマートフォンのいずれでもお問い合わせいただけます。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_02.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">ヒアリング・打ち合わせ</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 02</p>
                    <p>お客さまのご要望をうかがい、現地を拝見したうえで見積を作成します。お見積りは無料です。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_03.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">プラン作成のお申込み</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 03</p>
                    <p>作成したプランの修正などを行い、最終的なお見積りをご提示いたします。ご了承いただいた内容に沿ってお手続きを進めます。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_04.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">ご契約</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 04</p>
                    <p>契約書に同意をいただいたうえで契約を締結し、工事の準備を進めます。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_05.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">着工（工事開始）</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 05</p>
                    <p>プランに従って施工を行います。工事に関するご相談があればいつでもご連絡ください。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_06.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">お引き渡し</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 06</p>
                    <p>お客さま立ち合いのもと最終確認を実施し、お引き渡しを行います。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_07.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">アフターメンテナンス</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 07</p>
                    <p>施工後も、気になる箇所があればいつでもご連絡ください。内容に応じてメンテナンスのご案内をいたします。</p>
                </div>
            </div>
        
        </div>
    </div>
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>