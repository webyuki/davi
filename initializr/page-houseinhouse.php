<?php get_header(); ?>

<main>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_house_fv.jpg">
        <div class="bgWhiteTrans paddingW">
            <div class="container" data-aos="fade-up">
                <div class="text-center">
                    <p class="fontEn h3 mb0 mainColor">HOUSE IN HOUSE</p>
                    <h3 class="h2 bold">ハウスインハウス</h3>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">WHAT IS HOUSE IN HOUSE?</p>
            <h3 class="h3 bold">ハウスインハウスとは？</h3>
        </div>
        <div class="row mb10" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_house_01.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">今の住まいを壊さないから、<br class="pc">リフォームがお手軽に</span></h4>
                <div class="mb30">

<p>「もっと簡単なリフォームで、暮らしの快適さを高めることはできないか？」そんな発想から生まれたのが、ハウスINハウスです。</p>

<p>快適な暮らしを送るために特に気になるのが、家の中の暑さや寒さ。</p>
<p>「断熱リフォーム」を施すことで、夏涼しく、冬は暖かい快適な住まいを実現することができます。</p>
<p>しかし従来の断熱リフォーム工事は、壁や床を一度壊す大掛かりなものが一般的で、費用も高額になりがちでした。</p>

                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-7">
                <div class="">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_house_02.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-5">
                <h5 class="bold h4 mainColorLight mb10">「お家の中にお家をつくる」リフォーム方法</h5>
                <div class="mb30">

<p>天井・壁・床にオリジナルパネルを貼り込み、内側からしっかりと覆うことで、涼しさや暖かさを逃さず、高い断熱性能を発揮します。</p>
<p>また家全体ではなく、生活エリアのみなど必要なところにだけ手を入れる部分的な施工も可能です。</p>

<p>“手軽さ”と“快適さ”を両立させ、家を壊さず「新築品質」にまで性能を引き上げる。</p>
<p>それがハウスINハウスのリフォームの新発想です。</p>


                </div>
            </div>
        </div>
    </div>
</section>


<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">ADVANTAGE</p>
            <h3 class="h3 bold">ハウスインハウスの魅力</h3>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_house_03.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">最短10日。短い工期</span></h4>
                <div class="mb30">

<p>ハウスINハウスは今の住まいを壊さないから、工期の短縮を実現。最短10日でスピーディーに工事を完了させることもできます。 </p>
<p>また、必要な部分だけリフォームができるので、住みながらの工事も可能です。</p>
<p>仮住まいを探す手間・費用にわずらわされることもないので、ご家族への負担を大きく軽減します。</p>



                </div>
            </div>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_house_04.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">基本パック498万円（税別）<br class="pc">わかりやすい値段設定で安心</span></h4>
                <div class="mb30">

<p>ハウスINハウスでは、特に要望の多い「キッチン・洗面所・バス・トイレ」の水回りを含む、室内23.5 畳の断熱リフォームを基本パックとして、498 万円（税別）からご提供しています。</p>
<p>（※フローリングやクロスの張り替えはもちろん、間仕切り変更工事や、設備の交換工事など、一通りの工事を含んだ基本パックです）</p>
                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_about_feature_01.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">メリット盛りだくさんの<br class="pc">断熱リフォーム</span></h4>
                <div class="mb30">

<p>“断熱性能”は住まいの快適さを左右する大切な要素。</p>
<p>新築住宅では2020年から義務化される断熱ですが、従来の家でその基準を満たしているのはわずか5%ほど。</p>
<p>しかし既存の家でも、リフォームによって断熱を実現することが可能です。</p>
<p>家の性能を向上させ、住まいの快適さを新築同様のレベルにまで引き上げるのが「断熱リフォーム」です。</p>


                </div>
            </div>
        </div>
    </div>
</section>


<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_reform_voice_bg.jpg">
        <div class="bgWhiteTrans padding">
            <div class="container" data-aos="fade-up">
                <div class="text-center mb50">
                    <p class="fontEn h4 mb0 mainColor">VOICE</p>
                    <h3 class="h3 bold">お客様の声</h3>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/oage_reform_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">キッチンから家族の様子を感じる事が出来るのが一番の魅力です。</h5>
                                <p class="grayLight bold">岡山市 N様</p>
                            </div>
                            <p class="text_m gray">さえぎるものがないのでキッチンから家族の様子を感じることができるのが一番の魅力です。家が完成し、これから家族や友人と集まって料理を楽しめたらいいなと思っていました。これからもいろいろな楽しみが生まれていきそうです。</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/oage_reform_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">キッチンから家族の様子を感じる事が出来るのが一番の魅力です。</h5>
                                <p class="grayLight bold">岡山市 N様</p>
                            </div>
                            <p class="text_m gray">さえぎるものがないのでキッチンから家族の様子を感じることができるのが一番の魅力です。家が完成し、これから家族や友人と集まって料理を楽しめたらいいなと思っていました。これからもいろいろな楽しみが生まれていきそうです。</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/oage_reform_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">キッチンから家族の様子を感じる事が出来るのが一番の魅力です。</h5>
                                <p class="grayLight bold">岡山市 N様</p>
                            </div>
                            <p class="text_m gray">さえぎるものがないのでキッチンから家族の様子を感じることができるのが一番の魅力です。家が完成し、これから家族や友人と集まって料理を楽しめたらいいなと思っていました。これからもいろいろな楽しみが生まれていきそうです。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>