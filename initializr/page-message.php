<?php get_header(); ?>

<main>

<section class="bgMainColor pageHeader">
	<div class="container">
		<div class="text-center white" data-aos="fade-up" >
			<div class="inlineBlock">
				<p class="fontEn h00 titleBdWhite mb30">MESSAGE</p>
				<h3 class="h3">メッセージ</h3>
			</div>
		</div>
	</div>
</section>

<section class="margin">
	<div class="container">
		<div class="text-center mainColor mb50" data-aos="fade-up" >
			<div class="inlineBlock">
				<p class="fontEn h00 titleBd mb30">THOUGHT</p>
				<h3 class="h3">代表の想い</h3>
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-6" data-aos="fade-right" >
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_message_01.jpg" alt="">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<h4 class="h3 bold mainColor mb30">品質が守る、岡田精工の信頼。</h4>
				<div>
				
<p>昭和57年、品質管理にこだわった現会長の岡田昭治によって、岡田精工は設立されました。岡山県内や周辺地域で需要の高い農機具にはじまり、現在は産業機械や医療機器も製造しています。</p>

<p>私たちは事業を拡大する過程で、お客さまが数あるメーカーのなかから岡田精工を選んでくださった理由を、いつも真正面から考えてきました。</p>

<p class="">そしてたくさんのお客さまやものづくりに携わる人とお話しし、利益だけを優先した材料選びをしないこと。どんな小さなご依頼にも、誠意をもって技術の限界まで挑むことが、信頼の理由であることを確信しました。</p>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6" data-aos="fade-left" >
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_message_01_1.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<div>
				
<p>現在は技術が発達し、どのメーカーでも同じような製品が作れるようになりました。それでも製品の品質を限界まで高めることができるのは、人の想いであり蓄積された技術です。</p>

<p class="mb50">私たちは、岡田精工が磨いてきた高い品質でお客さまのご要望にお応えすることで、これまで築いた信頼を守っていきたいと考えています。</p>


<p class="text_m mb0">代表取締役</p>
<p class="h3">岡田 哲治</p>

			
				</div>
			</div>
		</div>
	</div>
</section>


<section class="relative pc">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_message_02.jpg">
		<div class="paddingW">
			<div class="padding"></div>
		</div>
	</div>
</section>

<section class="relative sp">
	<div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_message_02.jpg')">
		<div class="paddingW">
			<div class="padding"></div>
		</div>
	</div>
</section>

<section class="padding bgGrid">
	<div class="container">
		<div class="text-center mainColor mb30" data-aos="fade-up">
			<div class="inlineBlock">
				<p class="fontEn h00 titleBd mb30">CONCEPT</p>
				<h3 class="h3">品質を支えるのは、知識・技術、そして人材。</h3>
			</div>
		</div>
		<div class="width720 mb50">
		
<p>岡田精工は、農業機械の製造で蓄積した鉄加工の知識と磨き上げた職人の技術によって、お客さまからの信頼に応える製品をご提供しています。</p>
		</div>
		<div class="row mb30">
			<div class="col-sm-6" data-aos="fade-right">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_message_03.jpg" alt="">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<h4 class="h3 bold mainColor mb30">知識と技術による確実な基礎</h4>
				<div>
				
<p>岡田精工を二代に渡って継続するなかで実感したのは、この会社にはまだまだたくさんの知識と技術を吸収する必要があるということ。新しい道はいつも、私たちがまだ向き合ったことのない課題を超えたところにひらけました。</p>

<p>その昔、採算が合わないことを覚悟して向き合った新製品の開発は、全国に広がる人とのつながりや、会社としてお客さまの信頼に応えることの意義を教えてくれました。弊社では知識と技術こそ、岡田精工の基礎と考えています。</p>			
				</div>
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_message_04.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<h4 class="h3 bold mainColor mb30">精度とスピードを重視した製造工程の向上</h4>
				<div>
				
<p>お客さまのご要望にお応えすることと、技術改善の両面から、弊社では生産効率の向上を常に意識しています。10秒を5秒に縮めること、そしてさらにその先。弊社の価値は高い技術と同一線上にあると考え、精度とスピードの双方を追求してまいりました。</p>

<p>少量のご依頼から大量の注文まで。お客さまのご要望には、これからも安定した製品をご提供することでお応えしてまいります。</p>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6" data-aos="fade-right">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_message_05.jpg" alt="">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<h4 class="h3 bold mainColor mb30">職人の技術向上と人材育成</h4>
				<div>
				
<p>どれだけ科学技術が発達し、機械が人に代わることができるようになっても、ものづくりには職人の経験と知識が不可欠です。岡田精工では弊社がこれまで蓄積した技術を次世代へ伝え、自ら前向きに知識を吸収し、技術を向上していける人材育成に努めています。</p>

<p>また、広くアジアや世界からも弊社の持つ製造技術を伝えていける人材を集め、長期的なビジョンに基づいた育成に取り組んでいます。</p>			
				</div>
			</div>
		</div>
	</div>
</section>

<section class="relative pc">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_message_02_1.jpg">
		<div class="paddingW">
			<div class="padding"></div>
		</div>
	</div>
</section>

<section class="relative sp">
	<div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_message_02.jpg')">
		<div class="paddingW">
			<div class="padding"></div>
		</div>
	</div>
</section>



<section class="margin">
	<div class="container">
		<div class="text-center mainColor mb50">
			<div class="inlineBlock">
				<p class="fontEn h00 titleBd mb30">COMPANY</p>
				<h3 class="h3">会社概要</h3>
			</div>
		</div>
		<div class="pageAboutCompanyUl width720 mb50" data-aos="fade-up">
			<img src="<?php echo get_template_directory_uri();?>/img/page_message_compa_01.jpg" alt="">
			<ul>
				<li>会社名</li>
				<li>岡田精工 株式会社</li>
			</ul>
			<ul>
				<li>所在地</li>
				<li>〒719-1102　岡山県総社市東阿曽1677-1</li>
			</ul>
			<ul>
				<li>連絡先</li>
				<li>TEL：086-208-6050　FAX：(0866)99-9138</li>
			</ul>
			<ul>
				<li>創業設立	</li>
				<li>昭和57年8月</li>
			</ul>
			<ul>
				<li>資本金	</li>
				<li>1,000万円</li>
			</ul>
			<ul>
				<li>代表者	</li>
				<li>代表者	代表取締役会長　岡田 昭治<br>代表取締役社長　岡田 哲治</li>
			</ul>
			<ul>
				<li>従業員数</li>
				<li>38名</li>
			</ul>
			<ul>
				<li>事業内容</li>
				<li>各種歯車・シャフト類の切削、マシニング・ガンドリル加工
</li>
			</ul>
			<ul>
				<li>敷地面積</li>
				<li>4,700坪</li>
			</ul>
			<ul>
				<li>工場面積</li>
				<li>780坪</li>
			</ul>
		</div>
		<div class="width720">
<iframe class="company_map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d52479.06137882257!2d133.75926931188968!3d34.7066595588603!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb65537e03e5c190e!2z5bKh55Sw57K-5bel77yI5qCq77yJ!5e0!3m2!1sja!2sjp!4v1551607671701" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>