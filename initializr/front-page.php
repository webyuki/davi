<?php get_header(); ?>


<script>
/*
var agent = window.navigator.userAgent.toLowerCase();
 
if(agent.indexOf('msie') > -1) {
 
  console.log('IEブラウザです');
 
} else if(agent.indexOf('edge') > -1) {
 
  console.log('Edgeブラウザです');
  
} else {
 
    $(document).ready(function() {
        $('#fullpage').fullpage({
            navigation: true,
            navigationPosition: 'right',
            scrollOverflow: true,
            controlArrows:true,
            slidesNavigation:true,
            anchors: ['fullfv', 'fullvisual', 'fullconcept', 'fullconcept2', 'fullconcept3','fullgallery','fullprice','fulloption','fulloption2','fullshop'],
            menu:".fullpageMenu",
            verticalCentered:true
        });
    });
 
}
*/

    $(document).ready(function() {
        $('#fullpage').fullpage({
            navigation: true,
            navigationPosition: 'right',
            scrollOverflow: true,
            controlArrows:true,
            slidesNavigation:true,
            anchors: ['fullfv', 'fullvisual', 'fullconcept', 'fullconcept2', 'fullconcept3','fullgallery','fullprice','fulloption','fulloption2','fullshop'],
            menu:".fullpageMenu",
            verticalCentered:true,
            touchSensitivity:20
        });
    });


</script>

<section class="topFvSection relative section">
    <a class="logoLink" href="<?php echo home_url();?>"><p class="logo remove"></p></a>
    <!--<img src="<?php echo get_template_directory_uri();?>/img/fv_01.jpg" alt="">-->
    
    
    <div class="topFv relative bgImg">
    
        <!--
        <div class="main_imgBox">
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_01.jpg');"></div>
            <div visclass="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_02.jpg');"></div>
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_03.jpg');"></div>
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_04.jpg');"></div>
        </div>
        -->
        <div class="topFvNews flex white absolute">
            <div class="topFvNewsTitle">
                <p class="fontNum h4">NEWS</p>
            </div>
            <div class="topFvNewsListBox">
                <ul class="topFvNewsUl slickNewsUl text_m">

                    <?php
                        //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
                        $paged = get_query_var('page');
                        $args = array(
                            'post_type' =>  'post', // 投稿タイプを指定
                            'paged' => $paged,
                            'posts_per_page' => 6, // 表示するページ数
                            'orderby'=>'date',
                            'order'=>'DESC'
                                    );
                        $wp_query = new WP_Query( $args ); // クエリの指定 	
                        while ( $wp_query->have_posts() ) : $wp_query->the_post();
                            //ここに表示するタイトルやコンテンツなどを指定 
                        get_template_part('content-post-fv'); 
                        endwhile;
                        //wp_reset_postdata(); //忘れずにリセットする必要がある
                        wp_reset_query();
                    ?>		
                </ul>
            </div>
        </div>
    </div>
</section>



<script>
$('.slickNewsUl').slick({
    autoplay:true,
    dots:false,
    arrows:false,
    autoplaySpeed: 5000,
    speed:800,
    cssEase:'ease'
    
});


</script>



<section class="section relative" id="visual">
    <div class="absolute fullpageVisualTitle">
        <p class="text-center mainColor fontEn h1 mb10">Visual</p>
    </div>
    <div class="slideWrap pc">
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_01.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_02.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_03.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_04.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_05.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_06.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_07.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_08.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_new_09.jpg');"></div>
    </div>
    <div class="slideWrap sp">
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_01.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_02.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_09.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_08.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_04.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_03.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_06.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_07.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_22.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_05.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_11.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_21.jpg');"></div>
        <div class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_visual_sp_10.jpg');"></div>
    </div>
</section>


<script>

//画面高さ取得
h = $(window).height();
$('.slideWrap .bgImg').css('min-height', h + 'px');


$('.slideWrap').slick({
	autoplay: true,
    pauseOnFocus:true,
    pauseOnDotsHover:true,
	autoplaySpeed: 5000,
    dots: true,
    infinite: true,
    speed: 1000,
    fade: false,
    cssEase: 'ease',
    arrows:false,
    touchThreshold:400,
    swipeToSlide:true
});
</script>





<section id="concept" class="bgImg section" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_01.jpg');">
    <div class="bgBlackTrans fullpageInner">
        <div class="container">
            <div class="width720 topConceptBox relative bdHover" >
                <div class="topConceptBoxLineWrap relative">
                    <div class="topConceptBoxLine">
                        <div class="text-center mb50">
                            <p class="mainColor fontEn h1">Fabric</p>
                        </div>
                        <div class="rolloverTranslate relative">
                            <div class="subColor fontEn mb30">
                                <p>We, at Davi have Sarto artisans who are skilled in su misura (complete order men’s clothing) finish all suits by hand. We propose you the best fabric according to your request and intention. The material that Sarto has in hand is the breath of the thread created in a quiet atelier. It gives you the sounds of hand-sewing and molding of iron work. When you put on the clothes, you will feel the best comfort through the Sarto’s work.</p>
                            </div>
                        </div>
                        <!--
                        <div class="text-center">
                            <button data-iziModal-open=".iziModal" class="button buttonSm h5 fontEn tra text-center">JP</button>
                        </div>
                        -->
                        <div class="text-center">
                            <button data-iziModal-open=".iziModal" class="button buttonClose serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                        </div>

                        <div class="iziModal bgBlack iziModalBox">
                            <div class="subColor serif">
                                <p>Davi（ダヴィ） では、一着のスーツをすべて手縫いで仕上げる”su misura（完全注文の紳士服）”の技術を持つサルト（職人）が、用途やご希望に合わせた生地をご提案いたします。</p>
                                <p>サルトが手にする生地から伝わるのは、静謐なアトリエで生まれる糸の息遣いと、アイロンワークによる熱の造形。体温を帯びる瞬間に極上の着心地を感じさせる一着を、研ぎ澄まされたサルトの手でお選びいたします。</p>
                            </div>
                            <button data-izimodal-close="" class="icon-close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="bgImg section" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_02.jpg');">
    <div class="bgBlackTrans fullpageInner">
        <div class="container">
            <div class="width720 topConceptBox relative bdHover" >
                <div class="topConceptBoxLineWrap relative">
                    <div class="topConceptBoxLine">
                        <div class="text-center mb50">
                            <p class="mainColor fontEn h1">Measurement</p>
                        </div>
                        <div class="rolloverTranslate relative">
                            <div class="subColor fontEn mb30">
                                <p>In Davi, Sarto who has refined the technique of su misura measures and performs fitting twice: temporary and intermediate stitching. Softness and lightness are achieved through technology that measures and corrects the unevenness of the body in millimeters. It brings out your individuality.</p>
                            </div>
                        </div>
                        <div class="text-center">
                            <button data-iziModal-open=".iziModalBox2" class="button buttonClose serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                        </div>
                        <div class="iziModal bgBlack iziModalBox2">
                            <div class="subColor serif">
                                <p>Daviでは、仮縫いと中縫いという2度のフィッティングによって”su misura”の技術を磨いたサルトが採寸を行います。</p>
                                <p>身体の凹凸をミリ単位で採寸・補正する技術によって、柔らかさと軽さを実現。着る人が持つ個性を最大限に引き出します。</p>
                            </div>
                            <button data-izimodal-close="" class="icon-close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="sewing" class="bgImg section" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_03.jpg');">
    <div class="bgBlackTrans fullpageInner">
        <div class="container">
            <div class="width720 topConceptBox relative bdHover" >
                <div class="topConceptBoxLineWrap relative">
                    <div class="topConceptBoxLine">
                        <div class="text-center mb50">
                            <p class="mainColor fontEn h1">Sewing</p>
                        </div>
                        <div class="rolloverTranslate relative">
                            <div class="subColor fontEn mb30">
                                <p>Japan is one of the best manufacturing countries. We, at Davi is a part of it on the suits making field. We do cutting and sewing at one of the best factories in Japan. The artisans show no willingness to compromise. Our products bring out the charm of high-quality fabrics to the fullest and produce a dignified appearance.</p>
                            </div>
                        </div>
                        <div class="text-center">
                            <button data-iziModal-open=".iziModalBox3" class="button buttonClose serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                        </div>
                        <div class="iziModal bgBlack iziModalBox3">
                            <div class="subColor serif">
                                <p>世界有数の品質を誇るものづくり大国日本。スーツの製作においてその意志を受け継ぐDaviでは、国内屈指のファクトリーで裁断・縫製を行います。</p>
								<p>結集するのは、一切の妥協を許さない、匠の精神を持つ職人たち。本毛芯を使って「やさしく、あまく」縫われた逸品は、上質な生地の魅力を最大限に引き出し、品格ある佇まいを演出します。</p>
                            </div>
                            <button data-izimodal-close="" class="icon-close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section relative" id="gallery">
    <div class="absolute fullpageVisualTitle">
        <p class="text-center mainColor fontEn h1 mb10">Gallery</p>
    </div>
    <div class="slideWrap">
        <div class="bgImg img01" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_gallery_07.jpg');"></div>
        <div class="bgImg img02" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_gallery_01.jpg');"></div>
        <div class="bgImg img03" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_gallery_02.jpg');"></div>
        <div class="bgImg img04" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_gallery_03.jpg');"></div>
        <div class="bgImg img05" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_gallery_04.jpg');"></div>
        <div class="bgImg img06" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_gallery_06.jpg');"></div>
    </div>
</section>



<script>

//画面高さ取得
h = $(window).height();
$('.slideWrap .bgImg').css('min-height', h + 'px');

$('#gallery .slideWrap').slick({
	autoplay: true,
    pauseOnFocus:true,
    pauseOnDotsHover:true,
	autoplaySpeed: 5000,
    dots: true,
    infinite: true,
    speed: 1000,
    fade: false,
    cssEase: 'ease',
    arrows:false,
    touchThreshold:10,
    swipeToSlide:true
});
</script>

<section class="section margin" id="price">
    <div class="container">
        <div class="width980" >
            <div class="text-center">
                <!--<p class="mainColor fontEn h3 titleBd inlineBlock mb10">Price</p>-->
                <p class="mainColor fontEn h3 mb30">Price</p>
            </div>
            <div class="width720">
                <div class="subColor serif mb30">
                    <p>スタイルや流行にとらわれることなく、着用するたび身体に馴染み、味わいを深めるオーダースーツ。オーナーだけが手にできる、唯一無二の逸品を仕立てます。</p>
                </div>
                <!--
                <p class="text-center mainColor mb10"><span class="fontNum fontNum600 h3">105,000</span><span class="h5"> 円〜(税別)</span></p>
				<p class="text-center mainColor serif mb50"><span class="fontEn h4">Delivery</span>  ........  約<span class="fontEn h4">40</span>日</p>
                -->
                <ul class="topPriceDottedUl mainColor text-center">
                    <li class="fontEn h4">Suit</li>
                    <li class="serif"><hr></li>
                    <li class="fontEn h4">¥105,000~<span class="h5">+tax</span></li>
                </ul>
                <ul class="topPriceDottedUl mainColor text-center">
                    <li class="fontEn h4">Coat</li>
                    <li class="serif"><hr></li>
                    <li class="fontEn h4">¥126,000~<span class="h5">+tax</span></li>
                </ul>
                <ul class="topPriceDottedUl mainColor text-center">
                    <li class="fontEn h4">Shirt</li>
                    <li class="serif"><hr></li>
                    <li class="fontEn h4">¥10,000~<span class="h5">+tax</span></li>
                </ul>
                <ul class="topPriceDottedUl mainColor text-center mb20">
                    <li class="fontEn h4">Tie</li>
                    <li class="serif"><hr></li>
                    <li class="fontEn h4">¥12,000~<span class="h5">+tax</span></li>
                </ul>
                <ul class="topPriceDottedUl mainColor text-center">
                    <li class="fontEn h4">Delivery</li>
                    <li class="serif"><hr></li>
                    <li class="fontEn h4">40<span class="h5">Days</span></li>
                </ul>
            </div>
            <!--
            <div class="text-center mb50">
                <button data-iziModal-open=".iziModalBox4" class="button buttonSm serif h5 tra text-center">取り扱い生地</button>
            </div>
            -->
            <div class="text-center mb20">
                <h5 class="titleBdW mainColor serif h4">取り扱い生地</h5>
            </div>
            <div class="text-center mb30">
                <button data-iziModal-open=".iziModalBox4" class="button buttonClose serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
            </div>
            <div class="iziModal bgBlack iziModalBox4">
                <div class="">
                    <div class="text-center mb30">
                        <h5 class="titleBdW mainColor serif h4">取り扱い生地</h5>
                    </div>
                    
                    <ul class="text-center subColor topPriceSubUl inlineBlockUl serif">
                        <li>HARRISONS OF EDINBURGH</li>
                        <li>William Halstead</li>
                        <li>FOX BROTHERS</li>
                        <li>HARDY MINNIS</li>
                        <li>Taylor &amp; Lodge</li>
                        <li>Spence Bryson</li>
                        <li>PORTER &amp; HARDING</li>
                        <li>MARLING &amp; EVANS</li>
                        <li>LORO PIANA</li>
                        <li>Ermenegildo Zegna</li>
                        <li>CARLO BARBERA</li>
                        <li>DRAGO</li>
                        <li>CANONICO</li>
                        <li>E.THOMAS</li>
                        <li>FRATELLI TALLIA DI DELFINO</li>
                        <li>CERRUTI</li>
                        <li>DORMEUIL</li>
                    </ul>
                </div>
                <button data-izimodal-close="" class="icon-close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>

            <div class="text-center mb20">
                <h5 class="titleBdW mainColor serif h4">サービス</h5>
            </div>
            <div class="text-center">
                <button data-iziModal-open=".iziModalBox5" class="button buttonClose serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
            </div>
            <div class="iziModal bgBlack iziModalBox5">
                <div class="">
                    <div class="text-center mb30">
                        <h5 class="titleBdW mainColor serif h4">下記のサービスは料金に含まれています</h5>
                    </div>
                    <ul class="text-center subColor topPriceSubUl inlineBlockUl serif">
                        <li>前面エッジピックステッチ</li>
                        <li>本毛芯使用</li>
                        <li>本切羽</li>
                        <li>本水牛ボタン</li>
                        <li>キュプラ100％裏地</li>
                        <li>パンツVスリット入り</li>
                        <li>Dカン留め</li>
                    </ul>
                </div>
                <button data-izimodal-close="" class="icon-close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>
            
        </div>
        
    </div>
</section>

<section class="margin section" id="option">
    <div class="container">
        <!--
        <div class="titleVetiLineWrap mb50" >
            <div class="relative">
                <div class="titleVetiLine">
                    <p class="text-center mainColor fontEn h1 mb10">Option</p>
                </div>
            </div>
        </div>
        -->
        <div class="width980 relative" >
            <div class="topPriceLineBox">
                <div class="">
                    <div class="text-center">
                        <p class="mainColor fontEn h3 mb30">Hand Made Option</p>
                    </div>
                    <div class="subColor serif width720 mb20 ">
                        <p>ご希望の箇所を熟練した職人が手作業で仕立てる、フルオーダースーツの深みを味わえるオプションを揃えました。</p>
                    </div>
                    <p class="text-center mainColor mb30 fontNum"><span class="fontNum600 h3">¥10,000~ </span><span class="h5">+tax</span></p>
                    
                    <ul class="topPriceUl inlineBlockUl text-center">
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>肩イセ</span>
                                <button data-iziModal-open=".iziModalOp01" class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            
                            <div class="iziModal bgBlack iziModalOp01">
                                <div class=" text-center">
                                    <p class="topHandMadeIzmodalText topPriceUlText serif subColor text_m mb10">肩イセを増すことで肩甲骨をカバーし、<br class="sp">前肩にゆとりを持たせ、<br class="sp">軽い着心地を実現します。<br>
                                        <span class="text_m grayLight">＊イセ・・・裁縫で布を縮めて膨らみや丸みを出す技法</span>
                                    </p>
                                </div>
                                <button data-izimodal-close="" class="icon-close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                            
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>袖イセ</span>
                                <button data-iziModal-open=".iziModalOp02" class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            
                            <div class="iziModal bgBlack iziModalOp02">
                                <div class=" text-center">
                                    <p class="topHandMadeIzmodalText topPriceUlText serif subColor text_m mb10">袖山のイセを増すことで、肩廻りの厚みが増し、可動域が広がり、運動量が増します。<br>
                                        <span class="text_m grayLight">＊イセ・・・裁縫で布を縮めて膨らみや丸みを出す技法</span>
                                    </p>
                                </div>
                                <button data-izimodal-close="" class="icon-close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>本衿かけ</span>
                                <button data-iziModal-open=".iziModalOp03" class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <div class="iziModal bgBlack iziModalOp03">
                                <div class=" text-center">
                                    <p class="topHandMadeIzmodalText topPriceUlText serif subColor text_m mb10">クセ取りをして、上襟端の横地の目を通します。上衿のゆとりを入れることでのぼり衿にします。<br>
                                        <span class="text_m grayLight">＊クセ取り・・・生地の伸縮を利用して、アイロンワークで布を立体的に変形させる技法</span>
                                    </p>
                                </div>
                                <button data-izimodal-close="" class="icon-close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>胸ボリューム増し</span>
                                <button data-iziModal-open=".iziModalOp04" class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <div class="iziModal bgBlack iziModalOp04">
                                <div class=" text-center">
                                    <p class="topHandMadeIzmodalText topPriceUlText serif subColor text_m">毛芯を特殊加工することで胸のボリュームを増やし、美しいシルエットを形成します。</p>
                                </div>
                                <button data-izimodal-close="" class="icon-close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>パンツクセ取り</span>
                                <button data-iziModal-open=".iziModalOp05" class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <div class="iziModal bgBlack iziModalOp05">
                                <div class=" text-center">
                                    <p class="topHandMadeIzmodalText topPriceUlText serif subColor text_m">裁縫前後にクセを取ることで、脚の形に近い美しいシルエットを作ります。<br>
                                        <span class="text_m grayLight">＊クセ取り・・・生地の伸縮を利用して、アイロンワークで布を立体的に変形させる技法</span>
                                    </p>
                                </div>
                                <button data-izimodal-close="" class="icon-close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>カーブベルト</span>
                                <button data-iziModal-open=".iziModalOp06" class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <div class="iziModal bgBlack iziModalOp06">
                                <div class=" text-center">
                                    <p class="topHandMadeIzmodalText topPriceUlText serif subColor text_m">麻芯をクセ取りし、カーブをつけることでベルトを腰にフィットさせます。身頃のゆとりも増え、より良い履き心地を実現します。</p>
                                </div>
                                <button data-izimodal-close="" class="icon-close">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>ハンドステッチ</span>
                                <button data-iziModal-open=".iziModalOp06" class="button buttonClose sm serif h5 tra text-center visibleHidden"><i class="buttonCloseIco"></i></button>
                            </h4>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>ハンドボタンホール</span>
                                <button data-iziModal-open=".iziModalOp06" class="button buttonClose sm serif h5 tra text-center visibleHidden"><i class="buttonCloseIco"></i></button>
                            </h4>
                        </li>
                    </ul>

                    <!--
                    <ul class="topPriceUl inlineBlockUl mb30">
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>肩イセ</span>
                                <button class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <p class="topPriceUlText serif subColor text_m">肩イセを増すことで肩甲骨をカバーし、前肩にゆとりを持たせ、軽い着心地を実現します。<br>
                                <span class="text_m grayLight">＊イセ・・・裁縫で布を縮めて膨らみや丸みを出す技法</span>
                            </p>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>袖イセ</span>
                                <button class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <p class="topPriceUlText serif subColor text_m">袖山のイセを増すことで、肩廻りの厚みが増し、可動域が広がり、運動量が増します。<br>
                                <span class="text_m grayLight">＊イセ・・・裁縫で布を縮めて膨らみや丸みを出す技法</span>
                            </p>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>本衿かけ</span>
                                <button class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <p class="topPriceUlText serif subColor text_m">クセ取りをして、上襟端の横地の目を通します。上衿のゆとりを入れることでのぼり衿にします。<br>
                                <span class="text_m grayLight">＊クセ取り・・・生地の伸縮を利用して、アイロンワークで布を立体的に変形させる技法</span>
                            </p>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>胸ボリューム増し</span>
                                <button class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <p class="topPriceUlText serif subColor text_m">毛芯を特殊加工することで胸のボリュームを増やし、美しいシルエットを形成します。</p>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>パンツクセ取り</span>
                                <button class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <p class="topPriceUlText serif subColor text_m">裁縫前後にクセを取ることで、脚の形に近い、美しいシルエットを作ります。<br>
                                <span class="text_m grayLight">＊クセ取り・・・生地の伸縮を利用して、アイロンワークで布を立体的に変形させる技法</span>
                            </p>
                        </li>
                        <li>
                            <h4 class="topPriceUlTitle wButton topServiceTitle serif subColor relative h4">
                                <span>カーブベルト</span>
                                <button class="button buttonClose sm serif h5 tra text-center"><i class="buttonCloseIco"></i></button>
                            </h4>
                            <p class="topPriceUlText serif subColor text_m">麻芯をクセ取りし、カーブをつけることでベルトを腰にフィットさせます。身頃のゆとりも増え、より良い履き心地を実現します。</p>
                        </li>
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>ハンドステッチ</span></h4>
                        </li>
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>ハンドボタンホール</span></h4>
                        </li>
                    </ul>
                    -->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="margin section" id="">
    <div class="container">
        <div class="width980 relative" >
            <div class="topPriceLineBox">
                <div class="">
                    <div class="text-center">
                        <p class="mainColor fontEn h3 mb30">Sartoria Option</p>
                    </div>
                    <div class="subColor serif width720 mb20 ">
                        <p>サルトリア（仕立て屋）が提案する、ハンドメイドスーツの真髄を凝縮したオプションセット。この他にも多数のオプションを揃えています。</p>
                    </div>
                    <p class="text-center mainColor mb30 fontNum"><span class="fontNum600 h3">¥15,000 </span><span class="h5">+tax</span></p>
                    <ul class="topPriceUl inlineBlockUl text-center">
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>オールピックステッチ</span></h4>
                        </li>
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>見返し台場仕様</span></h4>
                        </li>
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>ナポリ袖</span></h4>
                        </li>
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>パンツ脇ピックステッチ</span></h4>
                        </li>
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>衿穴ハンドホール</span></h4>
                        </li>
                        <li>
                            <h4 class="topServiceTitle serif subColor relative h4"><span>フラワーループ</span></h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>



<script>
/*オプションの開閉*/
/*
$(function(){
	$(".topPriceUlTitle").on("click", function() {
		$(this).next(".topPriceUlText").slideToggle();
		$(this).find('.button').toggleClass("hover");
	});
});
*/
</script>

<script>
$(function(){
	$(".iziModal").iziModal({
        padding:"10% 5%",
        width:"90%",
        overlayColor: "rgba(0, 0, 0, 0.8)",
        closeButton:true,
        fullscreen: true, //全画面表示
        transitionIn: 'fadeInUp', //表示される時のアニメーション
        transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
    });
})
</script>




<?php get_footer(); ?>